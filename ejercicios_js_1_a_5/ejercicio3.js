var letter = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
var dni;
var letterIn;
var letterCalc;

function inputDni() {
    dni = prompt('Introduce el número del DNI');
    letterIn = prompt('Introduce la letra').toUpperCase();

    if ((dni >= 0) && (dni <= 99999999)) {
        letterCalc = letter[dni % 23];
        if (letterIn == letterCalc) {
            console.log('DNI Válido');
        } else {
            console.log('DNI Incorrecto');
        }
    } else {
        console.log('El número proporcionado no es válido ');
    }
}

inputDni();